#include <stdio.h>
#include "44b.h"
#include "leds.h"
#include "utils.h"
#include "D8Led.h"
#include "intcontroller.h"
#include "timer.h"
#include "gpio.h"
#include "uart.h"
#include "keyboard.h"

#define N 4 //Tamaño del buffer tmrbuffer
#define M 128 //Tamaño del buffer readlineBuf que se pasa como par�metro a la rutina readline

/* Variables para la gestión de la ISR del teclado
 * 
 * Keybuffer: puntero que apuntará al buffer en el que la ISR del teclado debe
 *            almacenar las teclas pulsadas
 * keyCount: variable en el que la ISR del teclado almacenará el número de teclas pulsadas
 */
volatile static char *keyBuffer = NULL;
volatile static int keyCount = 0;

/* Variables para la gestion de la ISR del timer
 * 
 * tmrbuffer: puntero que apuntar� al buffer que contendr� los d�gitos que la ISR del
 *            timer debe mostrar en el display de 8 segmentos
 * tmrBuffSize: usado por printD8Led para indicar el tama�o del buffer a mostrar
 */
volatile static char *tmrBuffer = NULL;
volatile static int tmrBuffSize = 0;

//Variables globales para la gestión del juego
static char passwd[N];  //Buffer para guardar la clave inicial
static char guess[N];   //Buffer para guardar la segunda clave
char readlineBuf[M];    //Buffer para guardar la linea leída del puerto serie

//Configuración de la uart
struct ulconf uconf = {
	.ired = OFF,
	.par  = NONE,
    .stopb = ONE,
	.wordlen = EIGHT,
	.echo = ON,
	.baud    = 115200,
};

enum state {
	INIT = 0,     //Init:       Inicio del juego
	SPWD = 1,     //Show Pwd:   Mostrar password
	DOGUESS = 2,  //Do guess:   Adivinar contraseña
	SGUESS = 3,   //Show guess: Mostrar el intento
	GOVER = 4     //Game Over:  Mostrar el resultado
};
enum state gstate; //estado/fase del juego 

// Declaración adelantada de las ISRs de timer y teclado (las marca como ISRs)
// Código de la parte1
void timer_ISR(void) __attribute__ ((interrupt ("IRQ")));
void keyboard_ISR(void) __attribute__ ((interrupt ("IRQ")));

// Función que va guardando las teclas pulsadas
static void push_buffer(char *buffer, int key)
{
	int i;
	for (i=0; i < N-1; i++)
		buffer[i] = buffer[i+1];
	buffer[N-1] = (char) key;
}

void timer_ISR(void)
{
	static int pos = 0; //contador para llevar la cuenta del dígito del buffer que toca mostrar

    // Visualizar el dígito en la posición pos del buffer tmrBuffer en el display

	// Only for debugging
	D8Led_init();
	Delay(500);
	D8Led_digit(tmrBuffer[pos]);

	// Si es el último dígito:
	//      Poner pos a cero,
	//      Parar timer
	//      Dar tmrBuffer valor NULL
	if (pos == tmrBuffSize-1) {
		pos = 0;
		tmr_stop(TIMER0);
		tmrBuffer = NULL;
	} else {
		pos++;
	}

	// Si no, se apunta al siguiente dígito a visualizar (pos)

	// Finalizar correctamente la ISR
	ic_cleanflag(INT_TIMER0);
}

void printD8Led(char *buffer, int size)
{
	//Esta rutina prepara el buffer que debe usar timer_ISR (tmrBuffer)
	tmrBuffer = buffer;
	tmrBuffSize = size;
	
	// Arrancar el TIMER0
	tmr_start(TIMER0);

	// Esperar a que timer_ISR termine (tmrBuffer)
	while(tmrBuffer != NULL) {}
}

void keyboard_ISR(void)
{
	int key;

	/* Eliminar rebotes de presión */
	Delay(200);

	/* Escaneo de tecla */
	key = kb_scan();

	if (key != -1) {
		//Si la tecla pulsada es F deshabilitar interrupciones por teclado y
		//poner keyBuffer a NULL
		if (key == 0xF) {
			// PREGUNTAR: ¿Así debería funcionar?
			ic_disable(INT_EINT1);
			keyBuffer = NULL;
		} else {
			// Si la tecla no es F guardamos la tecla pulsada en el buffer apuntado
			// por keybuffer mediante la llamada a la rutina push_buffer
			push_buffer(keyBuffer, key);
			// Actualizamos la cuenta del número de teclas pulsadas
			keyCount++;
		}
		/* Esperar a que la tecla se suelte, consultando el registro de datos rPDATG */		
		while (!(rPDATG & 0x02));
	}

	/* Eliminar rebotes de depresión */
	Delay(200);

	// Finalizar correctamente la ISR
	ic_cleanflag(INT_EINT1);
}

int read_kbd(char *buffer)
{
	//Esta rutina prepara el buffer en el que keyboard_ISR almacenará las teclas
	//pulsadas (keyBuffer) y pone a 0 el contador de teclas pulsadas
	keyBuffer = buffer;
	keyCount = 0;

	// Habilitar interrupciones por teclado
	// PREGUNTAR: ¿Es necesario habilitar INT_GLOBAL?
	ic_enable(INT_EINT1);
	ic_enable(INT_GLOBAL);

	//Esperar a que keyboard_ISR indique que se ha terminado de
	//introducir la clave (keyBuffer)
	while (keyBuffer != NULL) {}

	//Devolver número de teclas pulsadas
	return keyCount;
}

int readline(char *buffer, int size)
{
	int count = 0; //cuenta del número de bytes leidos
	char c;        //variable para almacenar el carácter leído

	if (size == 0)
		return 0;

	// Leer caracteres de la uart0 y copiarlos al buffer
	// hasta que llenemos el buffer (size) o el carácter leído sea
	// un retorno de carro '\r'
	// Los caracteres se leen de uno en uno, utilizando la interfaz
	// del módulo uart, definida en el fichero uart.h
	do {
		uart_getch(UART0, &c);
		buffer[count++] = c;
	} while (c != '\r' && count < size);

	return count;
}

static int show_result()
{
	int error = 0;
	int i = 0;
	char buffer[2] = {0};

	// poner error a 1 si las contraseñas son distintas
	for (i = 0; i < N && !error; ++i) error = guess[i] != passwd[i];

	// Hay que visualizar el resultado durante 2s.
	// Si se ha acertado tenemos que mostrar una A y si no una E
	// Como en printD8Led haremos que la ISR del timer muestre un buffer con dos
	// caracteres A o dos caracteres E (eso durará 2s)
	for (i = 0; i < 2; ++i) buffer[i] = error?0xE:0xA;
	printD8Led(buffer, 2);
	
	// esperar a que la ISR del timer indique que se ha terminado
	while(tmrBuffer != NULL) {}

	// devolver el valor de error para indicar si se ha acertado o no
	return error;
}

int setup(void)
{

	D8Led_init();

	/* Configuración del timer0 para interrumpir cada segundo */
	tmr_set_prescaler(0, 255);
	tmr_set_divider(0,D1_8);
	// tmr_set_count(TIMER0, 62500, 62495);
	tmr_set_count(TIMER0, 31250, 31245);
	tmr_set_mode(TIMER0, RELOAD);

	tmr_update(TIMER0);
	tmr_stop(TIMER0);

	/***************************/
	/* Port G-configuración para generación de interrupciones externas
	 *         por teclado
	 **/
	portG_conf(1, EINT);
	portG_eint_trig(1, FALLING);
	portG_conf_pup(1, ENABLE);

	/********************************************************************/

	// Registramos las ISRs

	pISR_TIMER0 = (unsigned) timer_ISR;
	pISR_EINT1 = (unsigned) keyboard_ISR;

	/* Configuración del controlador de interrupciones*/
	ic_init();

	ic_conf_irq(ENABLE, VEC);
	ic_conf_fiq(DISABLE);

	ic_conf_line(INT_TIMER0, IRQ);
	ic_conf_line(INT_EINT1, IRQ);

	ic_enable(INT_TIMER0);
	ic_enable(INT_GLOBAL);
	/***************************************************/

	/***************************************************/
	//COMPLETAR: Configuración de la uart0
	
		/* Hay que:
		 * 1. inicializar el m�dulo
		 * 2. Configurar el modo linea de la uart0 usando la variable global uconf
		 * 3. Configurar el modo de recepción (POLL o INT) de la uart0
		 * 4. Configurar el modo de transmisión (POLL o INT) de la uart0
		 */

	/***************************************************/

	uart_init();
	uart_lconf(UART0, &uconf);
	uart_conf_txmode(UART0, INT);
	uart_conf_rxmode(UART0, INT);

	Delay(0);

	/* Inicio del juego */
	gstate = INIT;
	D8Led_digit(12);

	return 0;
}

static char ascii2digit(char c)
{
	char d = -1;

	if ((c >= '0') && (c <= '9'))
		d = c - '0';
	else if ((c >= 'a') && (c <= 'f'))
		d = c - 'a' + 10;
	else if ((c >= 'A') && (c <= 'F'))
		d = c - 'A' + 10;

	return d;
}


int loop(void)
{
	int count; //número de teclas pulsadas
	int error;

	//Máquina de estados

	switch (gstate) {
		case INIT:
			do {
				D8Led_digit(0xC);
				uart_send_str(UART0, "Enter password using onboard keyboard\n");
     			//Llamar a la rutina read_kbd para guardar los 4 dígitos en el buffer passwd
     			//Esta rutina devuelve el número de teclas pulsadas.
				//Dibujar una E en el display si el número de teclas pulsadas es menor que 4
				if ((count = read_kbd(passwd)) < 4) {
					D8Led_digit(0xE);
					Delay(10000);
				}
			} while (count < 4);

			//Pasar al estado siguiente
			gstate = SPWD;

			break;

		case SPWD:

			// Visualizar en el display los 4 dígitos del buffer passwd, para
			// ello llamar a la rutina printD8Led
			printD8Led(passwd, N);
			// Delay(10000);
			// Pasar al estado siguiente
			gstate = DOGUESS;
			break;

		case DOGUESS:
			Delay(10000);
			do {
				//COMPLETAR:
				/* 
				 * 1. Mandar por el puerto serie uart0 la cadena "Introduzca passwd: "
				 *    usando el interfaz definido en uart.h
				 *
				 * 2. Mostrar una F en el display
				 *
				 * 3. Llamar a la rutina readline para leer una l�nea del puerto
				 *    serie en el buffer readlineBuf, almacenando en count el
				 *    valor devuelto (n�mero de caracteres le�dos)
				 *
				 * 4. Si el �ltimo caracter le�do es un '\r' decrementamos count
				 *    para no tenerlo en cuenta
				 *
				 * 5. Si count es menor de 4 la clave no es v�lida, mostramos
				 *    una E (digito 14) en el display de 8 segmentos y esperamos
				 *    1 segundo con Delay.
				 */
				uart_send_str(UART0, "guess: ");
				D8Led_digit(15);
				count = readline(readlineBuf, M);

				if (readlineBuf[count-1] == '\r') {
					count--;
				}

				if (count < 4) {
					D8Led_digit(14);
					Delay(1000);
				}
			} while (count < 4);

			/* COMPLETAR: debemos copiar los 4 �ltimos caracteres de readline en
			 * el buffer guess, haciendo la conversi�n de ascii-hexadecimal a valor
			 * decimal. Para ello podemos utilizar la funci�n ascii2digit
			 * definida m�s arriba.
			 */
			int i;
			for (i = 0; i < 4; i++) {
				guess[i] = ascii2digit(readlineBuf[i]);
			}

			//Pasar al estado siguiente
			//Código de la parte1
			gstate = SGUESS;

			break;

		case SGUESS:
			//Visualizar en el display los 4 dígitos del buffer guess,
			//para ello llamar a la rutina printD8Led
			printD8Led(guess, N);
			// Delay(10000);
			//Pasar al estado siguiente
			gstate = GOVER;
			break;

		case GOVER:
			//Código de la parte1
			// Delay(10000);
			gstate = (error = show_result())?DOGUESS:INIT;
			if (gstate == DOGUESS) uart_send_str(UART0, "Failed, try new ");

			break;
	}
	return 0;
}

int main(void)
{
	setup();

	while (1) {
		loop();
	}
}
